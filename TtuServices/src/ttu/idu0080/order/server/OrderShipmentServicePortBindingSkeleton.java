/**
 * OrderShipmentServicePortBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class OrderShipmentServicePortBindingSkeleton implements ttu.idu0080.order.server.OrderShipmentService, org.apache.axis.wsdl.Skeleton {
    private ttu.idu0080.order.server.OrderShipmentService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "order_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ryhm_name"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOrderShipmentsByOrderIdRyhmName", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "orderShipment"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getOrderShipmentsByOrderIdRyhmName"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOrderShipmentsByOrderIdRyhmName") == null) {
            _myOperations.put("getOrderShipmentsByOrderIdRyhmName", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOrderShipmentsByOrderIdRyhmName")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "order_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ryhm_name"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "tracking_number"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "courier_name"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "approx_delivery_date"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "shipping_price"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"), double.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("insertOrderShipment", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "insertOrderShipment"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("insertOrderShipment") == null) {
            _myOperations.put("insertOrderShipment", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("insertOrderShipment")).add(_oper);
    }

    public OrderShipmentServicePortBindingSkeleton() {
        this.impl = new ttu.idu0080.order.server.OrderShipmentServicePortBindingImpl();
    }

    public OrderShipmentServicePortBindingSkeleton(ttu.idu0080.order.server.OrderShipmentService impl) {
        this.impl = impl;
    }
    public ttu.idu0080.order.server.OrderShipment[] getOrderShipmentsByOrderIdRyhmName(int order_id, java.lang.String ryhm_name) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.OrderShipment[] ret = impl.getOrderShipmentsByOrderIdRyhmName(order_id, ryhm_name);
        return ret;
    }

    public int insertOrderShipment(int order_id, java.lang.String ryhm_name, java.lang.String tracking_number, java.lang.String courier_name, java.util.Calendar approx_delivery_date, double shipping_price) throws java.rmi.RemoteException
    {
        int ret = impl.insertOrderShipment(order_id, ryhm_name, tracking_number, courier_name, approx_delivery_date, shipping_price);
        return ret;
    }

}
