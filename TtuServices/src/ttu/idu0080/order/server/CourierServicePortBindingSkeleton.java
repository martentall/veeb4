/**
 * CourierServicePortBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class CourierServicePortBindingSkeleton implements ttu.idu0080.order.server.CourierService, org.apache.axis.wsdl.Skeleton {
    private ttu.idu0080.order.server.CourierService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "county"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getCouriersByAddress", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "courier"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getCouriersByAddress"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getCouriersByAddress") == null) {
            _myOperations.put("getCouriersByAddress", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getCouriersByAddress")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getAllCouriers", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "courier"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getAllCouriers"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAllCouriers") == null) {
            _myOperations.put("getAllCouriers", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAllCouriers")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "courier_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getCourierById", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "courier"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getCourierById"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getCourierById") == null) {
            _myOperations.put("getCourierById", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getCourierById")).add(_oper);
    }

    public CourierServicePortBindingSkeleton() {
        this.impl = new ttu.idu0080.order.server.CourierServicePortBindingImpl();
    }

    public CourierServicePortBindingSkeleton(ttu.idu0080.order.server.CourierService impl) {
        this.impl = impl;
    }
    public ttu.idu0080.order.server.Courier[] getCouriersByAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Courier[] ret = impl.getCouriersByAddress(country, county);
        return ret;
    }

    public ttu.idu0080.order.server.Courier[] getAllCouriers() throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Courier[] ret = impl.getAllCouriers();
        return ret;
    }

    public ttu.idu0080.order.server.Courier getCourierById(int courier_id) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Courier ret = impl.getCourierById(courier_id);
        return ret;
    }

}
