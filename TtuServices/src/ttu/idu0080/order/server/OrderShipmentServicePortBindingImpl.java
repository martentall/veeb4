/**
 * OrderShipmentServicePortBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

import java.util.Calendar;

import ttu.idu0080.order.dao.OrderShipmentDao;

public class OrderShipmentServicePortBindingImpl implements ttu.idu0080.order.server.OrderShipmentService{
    public ttu.idu0080.order.server.OrderShipment[] getOrderShipmentsByOrderIdRyhmName(int order_id, java.lang.String ryhm_name) throws java.rmi.RemoteException {
        return null;
    }

    public int insertOrderShipment(int order_id, java.lang.String ryhm_name, java.lang.String tracking_number, java.lang.String courier_name, java.util.Calendar approx_delivery_date, double shipping_price) throws java.rmi.RemoteException {
        OrderShipment shipment = createShipment(order_id, ryhm_name, tracking_number, courier_name, approx_delivery_date, shipping_price);
		return new OrderShipmentDao().insert(shipment);
    }

	private OrderShipment createShipment(int order_id, String ryhm_name,
			String tracking_number, String courier_name,
			Calendar approx_delivery_date, double shipping_price) {
		OrderShipment shipment = new OrderShipment();
		shipment.setOrder(order_id);
		shipment.setRyhm_name(ryhm_name);
		shipment.setTracking_number(tracking_number);
		shipment.setCourier_name(courier_name);
		shipment.setApprox_delivery_date(approx_delivery_date);
		shipment.setShipping_price(shipping_price);
		return shipment;
	}

}
