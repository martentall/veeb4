/**
 * Order.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class Order  implements java.io.Serializable {
    private ttu.idu0080.order.server.Customer customer;

    private int order_id;

    private ttu.idu0080.order.server.OrderProduct[] order_products;

    private float price_total;

    private ttu.idu0080.order.server.Seller seller;

    private ttu.idu0080.order.server.Address shipping_address;

    public Order() {
    }

    public Order(
           ttu.idu0080.order.server.Customer customer,
           int order_id,
           ttu.idu0080.order.server.OrderProduct[] order_products,
           float price_total,
           ttu.idu0080.order.server.Seller seller,
           ttu.idu0080.order.server.Address shipping_address) {
           this.customer = customer;
           this.order_id = order_id;
           this.order_products = order_products;
           this.price_total = price_total;
           this.seller = seller;
           this.shipping_address = shipping_address;
    }


    /**
     * Gets the customer value for this Order.
     * 
     * @return customer
     */
    public ttu.idu0080.order.server.Customer getCustomer() {
        return customer;
    }


    /**
     * Sets the customer value for this Order.
     * 
     * @param customer
     */
    public void setCustomer(ttu.idu0080.order.server.Customer customer) {
        this.customer = customer;
    }


    /**
     * Gets the order_id value for this Order.
     * 
     * @return order_id
     */
    public int getOrder_id() {
        return order_id;
    }


    /**
     * Sets the order_id value for this Order.
     * 
     * @param order_id
     */
    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }


    /**
     * Gets the order_products value for this Order.
     * 
     * @return order_products
     */
    public ttu.idu0080.order.server.OrderProduct[] getOrder_products() {
        return order_products;
    }


    /**
     * Sets the order_products value for this Order.
     * 
     * @param order_products
     */
    public void setOrder_products(ttu.idu0080.order.server.OrderProduct[] order_products) {
        this.order_products = order_products;
    }

    public ttu.idu0080.order.server.OrderProduct getOrder_products(int i) {
        return this.order_products[i];
    }

    public void setOrder_products(int i, ttu.idu0080.order.server.OrderProduct _value) {
        this.order_products[i] = _value;
    }


    /**
     * Gets the price_total value for this Order.
     * 
     * @return price_total
     */
    public float getPrice_total() {
        return price_total;
    }


    /**
     * Sets the price_total value for this Order.
     * 
     * @param price_total
     */
    public void setPrice_total(float price_total) {
        this.price_total = price_total;
    }


    /**
     * Gets the seller value for this Order.
     * 
     * @return seller
     */
    public ttu.idu0080.order.server.Seller getSeller() {
        return seller;
    }


    /**
     * Sets the seller value for this Order.
     * 
     * @param seller
     */
    public void setSeller(ttu.idu0080.order.server.Seller seller) {
        this.seller = seller;
    }


    /**
     * Gets the shipping_address value for this Order.
     * 
     * @return shipping_address
     */
    public ttu.idu0080.order.server.Address getShipping_address() {
        return shipping_address;
    }


    /**
     * Sets the shipping_address value for this Order.
     * 
     * @param shipping_address
     */
    public void setShipping_address(ttu.idu0080.order.server.Address shipping_address) {
        this.shipping_address = shipping_address;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Order)) return false;
        Order other = (Order) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customer==null && other.getCustomer()==null) || 
             (this.customer!=null &&
              this.customer.equals(other.getCustomer()))) &&
            this.order_id == other.getOrder_id() &&
            ((this.order_products==null && other.getOrder_products()==null) || 
             (this.order_products!=null &&
              java.util.Arrays.equals(this.order_products, other.getOrder_products()))) &&
            this.price_total == other.getPrice_total() &&
            ((this.seller==null && other.getSeller()==null) || 
             (this.seller!=null &&
              this.seller.equals(other.getSeller()))) &&
            ((this.shipping_address==null && other.getShipping_address()==null) || 
             (this.shipping_address!=null &&
              this.shipping_address.equals(other.getShipping_address())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomer() != null) {
            _hashCode += getCustomer().hashCode();
        }
        _hashCode += getOrder_id();
        if (getOrder_products() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOrder_products());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOrder_products(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Float(getPrice_total()).hashCode();
        if (getSeller() != null) {
            _hashCode += getSeller().hashCode();
        }
        if (getShipping_address() != null) {
            _hashCode += getShipping_address().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Order.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "customer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_products");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_products"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "orderProduct"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seller");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seller"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "seller"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping_address");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "address"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
