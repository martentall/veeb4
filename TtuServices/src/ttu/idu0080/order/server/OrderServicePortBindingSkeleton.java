/**
 * OrderServicePortBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class OrderServicePortBindingSkeleton implements ttu.idu0080.order.server.OrderService, org.apache.axis.wsdl.Skeleton {
    private ttu.idu0080.order.server.OrderService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "customer_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOrdersByCustomerId", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getOrdersByCustomerId"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOrdersByCustomerId") == null) {
            _myOperations.put("getOrdersByCustomerId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOrdersByCustomerId")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "county"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOrdersByShippingAddress", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getOrdersByShippingAddress"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOrdersByShippingAddress") == null) {
            _myOperations.put("getOrdersByShippingAddress", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOrdersByShippingAddress")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "last_name"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOrdersByCustomerLastName", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getOrdersByCustomerLastName"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOrdersByCustomerLastName") == null) {
            _myOperations.put("getOrdersByCustomerLastName", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOrdersByCustomerLastName")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getAllOrders", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getAllOrders"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAllOrders") == null) {
            _myOperations.put("getAllOrders", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAllOrders")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "order_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getOrdersByOrderId", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "order"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "getOrdersByOrderId"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getOrdersByOrderId") == null) {
            _myOperations.put("getOrdersByOrderId", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getOrdersByOrderId")).add(_oper);
    }

    public OrderServicePortBindingSkeleton() {
        this.impl = new ttu.idu0080.order.server.OrderServicePortBindingImpl();
    }

    public OrderServicePortBindingSkeleton(ttu.idu0080.order.server.OrderService impl) {
        this.impl = impl;
    }
    public ttu.idu0080.order.server.Order[] getOrdersByCustomerId(int customer_id) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Order[] ret = impl.getOrdersByCustomerId(customer_id);
        return ret;
    }

    public ttu.idu0080.order.server.Order[] getOrdersByShippingAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Order[] ret = impl.getOrdersByShippingAddress(country, county);
        return ret;
    }

    public ttu.idu0080.order.server.Order[] getOrdersByCustomerLastName(java.lang.String last_name) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Order[] ret = impl.getOrdersByCustomerLastName(last_name);
        return ret;
    }

    public ttu.idu0080.order.server.Order[] getAllOrders() throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Order[] ret = impl.getAllOrders();
        return ret;
    }

    public ttu.idu0080.order.server.Order getOrdersByOrderId(int order_id) throws java.rmi.RemoteException
    {
        ttu.idu0080.order.server.Order ret = impl.getOrdersByOrderId(order_id);
        return ret;
    }

}
