/**
 * OrderShipmentServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public interface OrderShipmentServiceService extends javax.xml.rpc.Service {
    public java.lang.String getOrderShipmentServicePortAddress();

    public ttu.idu0080.order.server.OrderShipmentService getOrderShipmentServicePort() throws javax.xml.rpc.ServiceException;

    public ttu.idu0080.order.server.OrderShipmentService getOrderShipmentServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
