/**
 * CourierServicePortBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

import java.util.List;

import ttu.idu0080.order.dao.EnterpriseDAO;

public class CourierServicePortBindingImpl implements ttu.idu0080.order.server.CourierService{
    public Courier[] getCouriersByAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException {
        return null;
    }

    public Courier[] getAllCouriers() throws java.rmi.RemoteException {
    	List<Courier> courierList = new EnterpriseDAO().findAllCouriers();
    	return courierList.toArray(new Courier[courierList.size()]);
    }

    public Courier getCourierById(int courier_id) throws java.rmi.RemoteException {
        return new EnterpriseDAO().findById(courier_id);
    }
    
}
