/**
 * CourierService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public interface CourierService extends java.rmi.Remote {
    public ttu.idu0080.order.server.Courier[] getCouriersByAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Courier[] getAllCouriers() throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Courier getCourierById(int courier_id) throws java.rmi.RemoteException;
}
