/**
 * OrderProduct.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class OrderProduct  implements java.io.Serializable {
    private int order_product;

    private float price;

    private float price_total;

    private int product_count;

    private java.lang.String product_name;

    public OrderProduct() {
    }

    public OrderProduct(
           int order_product,
           float price,
           float price_total,
           int product_count,
           java.lang.String product_name) {
           this.order_product = order_product;
           this.price = price;
           this.price_total = price_total;
           this.product_count = product_count;
           this.product_name = product_name;
    }


    /**
     * Gets the order_product value for this OrderProduct.
     * 
     * @return order_product
     */
    public int getOrder_product() {
        return order_product;
    }


    /**
     * Sets the order_product value for this OrderProduct.
     * 
     * @param order_product
     */
    public void setOrder_product(int order_product) {
        this.order_product = order_product;
    }


    /**
     * Gets the price value for this OrderProduct.
     * 
     * @return price
     */
    public float getPrice() {
        return price;
    }


    /**
     * Sets the price value for this OrderProduct.
     * 
     * @param price
     */
    public void setPrice(float price) {
        this.price = price;
    }


    /**
     * Gets the price_total value for this OrderProduct.
     * 
     * @return price_total
     */
    public float getPrice_total() {
        return price_total;
    }


    /**
     * Sets the price_total value for this OrderProduct.
     * 
     * @param price_total
     */
    public void setPrice_total(float price_total) {
        this.price_total = price_total;
    }


    /**
     * Gets the product_count value for this OrderProduct.
     * 
     * @return product_count
     */
    public int getProduct_count() {
        return product_count;
    }


    /**
     * Sets the product_count value for this OrderProduct.
     * 
     * @param product_count
     */
    public void setProduct_count(int product_count) {
        this.product_count = product_count;
    }


    /**
     * Gets the product_name value for this OrderProduct.
     * 
     * @return product_name
     */
    public java.lang.String getProduct_name() {
        return product_name;
    }


    /**
     * Sets the product_name value for this OrderProduct.
     * 
     * @param product_name
     */
    public void setProduct_name(java.lang.String product_name) {
        this.product_name = product_name;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderProduct)) return false;
        OrderProduct other = (OrderProduct) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.order_product == other.getOrder_product() &&
            this.price == other.getPrice() &&
            this.price_total == other.getPrice_total() &&
            this.product_count == other.getProduct_count() &&
            ((this.product_name==null && other.getProduct_name()==null) || 
             (this.product_name!=null &&
              this.product_name.equals(other.getProduct_name())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOrder_product();
        _hashCode += new Float(getPrice()).hashCode();
        _hashCode += new Float(getPrice_total()).hashCode();
        _hashCode += getProduct_count();
        if (getProduct_name() != null) {
            _hashCode += getProduct_name().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderProduct.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "orderProduct"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_product");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_product"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_count");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_count"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
