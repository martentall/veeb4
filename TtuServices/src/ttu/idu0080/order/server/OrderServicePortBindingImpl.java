/**
 * OrderServicePortBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

import java.util.List;

import ttu.idu0080.order.dao.OrderDAO;

public class OrderServicePortBindingImpl implements ttu.idu0080.order.server.OrderService{
    public ttu.idu0080.order.server.Order[] getOrdersByCustomerId(int customer_id) throws java.rmi.RemoteException {
        return null;
    }

    public ttu.idu0080.order.server.Order[] getOrdersByShippingAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException {
        return null;
    }

    public ttu.idu0080.order.server.Order[] getOrdersByCustomerLastName(java.lang.String last_name) throws java.rmi.RemoteException {
        return null;
    }

    public ttu.idu0080.order.server.Order[] getAllOrders() throws java.rmi.RemoteException {
    	List<Order> orders = new OrderDAO().findAll();
    	Order[] ordersArr = new Order[orders.size()];
    	for (int i=0; i < orders.size(); i++) {
    		ordersArr[i] = orders.get(i);
		}
        return ordersArr;
    }

    public ttu.idu0080.order.server.Order getOrdersByOrderId(int order_id) throws java.rmi.RemoteException {
        return new OrderDAO().findById(order_id);
    }

}
