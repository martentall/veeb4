package ttu.idu0080.order.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import ttu.idu0080.order.server.Address;

public class AddressDAO extends DAO<Address>{

	@Override
	public String tablename() {
		return "address";
	}

	@Override
	public Address map(ResultSet resultSet) throws SQLException {
		Address address = new Address();
		address.setAddress(resultSet.getInt("address"));
		address.setCountry(resultSet.getString("country"));
		address.setCounty(resultSet.getString("county"));
		address.setTown_village(resultSet.getString("town_village"));
		address.setStreet_address(resultSet.getString("street_address"));
		address.setZipcode(resultSet.getString("zipcode"));
		return address;	
	}

}
