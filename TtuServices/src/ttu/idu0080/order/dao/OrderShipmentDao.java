package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ttu.idu0080.order.server.OrderShipment;

public class OrderShipmentDao extends DAO<OrderShipment> {

	private static final String INSERT_SQL = "INSERT INTO ORDER_SHIPMENT" 
			+ " (order_fk,courier_name,ryhm_name,shipping_price,tracking_number,approx_delivery_date) "
			+ " VALUES (?,?,?,?,?,?)";
	@Override
	public String tablename() {
		return "order_shipement";
	}
	
	public int insert(OrderShipment shipment) {
		try (Connection connection = connection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL)) {
			
			statement.setInt(1, shipment.getOrder());
			statement.setString(2, shipment.getCourier_name());
			statement.setString(3, shipment.getRyhm_name());
			statement.setDouble(4, shipment.getShipping_price());
			statement.setString(5, shipment.getTracking_number());
			statement.setDate(6, new java.sql.Date(shipment.getApprox_delivery_date().getTimeInMillis()));

			return statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public OrderShipment map(ResultSet resultSet) throws SQLException {
		OrderShipment shipment = new OrderShipment();
		shipment.setOrder_shipment(resultSet.getInt("order_shipment"));
		shipment.setOrder(resultSet.getInt("order_fk"));
		shipment.setCourier_name(resultSet.getString("courier_name"));
		shipment.setRyhm_name(resultSet.getString("ryhm_name"));
		shipment.setShipping_price(resultSet.getDouble("shipping_price"));
		shipment.setTracking_number(resultSet.getString("tracking_number"));
		shipment.setApprox_delivery_date(getApproxDelTime(resultSet));
		return shipment;
	}

	private Calendar getApproxDelTime(ResultSet resultSet) throws SQLException {
		Calendar cal = Calendar.getInstance();
		cal.setTime(resultSet.getDate("approx_delivery_date"));
		return cal;
	}

}
