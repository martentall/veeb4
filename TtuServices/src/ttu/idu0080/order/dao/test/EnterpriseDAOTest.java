package ttu.idu0080.order.dao.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ttu.idu0080.order.dao.EnterpriseDAO;

public class EnterpriseDAOTest {

	@Test
	public void testFindAllCouriers() {
		EnterpriseDAO dao = new EnterpriseDAO();
		assertEquals(8, dao.findAllCouriers().size());
	}
	
	@Test
	public void testFindById() {
		EnterpriseDAO dao = new EnterpriseDAO();
		assertEquals(1, dao.findById(1).getEnterprise());
	}
}
