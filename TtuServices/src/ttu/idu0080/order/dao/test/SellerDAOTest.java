package ttu.idu0080.order.dao.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ttu.idu0080.order.dao.SellerDAO;
import ttu.idu0080.order.server.Seller;

public class SellerDAOTest {

	@Test
	public void testFindAllCouriers() {
		SellerDAO dao = new SellerDAO();
		assertEquals(2, dao.findAll().size());
	}

	@Test
	public void testMap() {
		SellerDAO dao = new SellerDAO();
		Seller seller = dao.findAll().get(0);
		assertNotNull(seller.getEnterprise());
		assertNotNull(seller.getName());
	}
}
