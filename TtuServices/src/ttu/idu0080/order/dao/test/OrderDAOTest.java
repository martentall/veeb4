package ttu.idu0080.order.dao.test;

import static org.junit.Assert.*;

import org.junit.Test;

import ttu.idu0080.order.dao.OrderDAO;
import ttu.idu0080.order.server.Order;

public class OrderDAOTest {

	@Test
	public void testFindAll() {
		OrderDAO orderDAO = new OrderDAO();
		assertTrue(!orderDAO.findAll().isEmpty());
	}

	@Test
	public void shouldFindById() {
		OrderDAO orderDAO = new OrderDAO();
		Order order = orderDAO.findById(1);
		assertNotNull(order);
		assertNotNull(order.getShipping_address().getAddress());
	}
}
