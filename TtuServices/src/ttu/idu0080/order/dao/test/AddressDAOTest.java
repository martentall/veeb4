package ttu.idu0080.order.dao.test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import ttu.idu0080.order.dao.AddressDAO;
import ttu.idu0080.order.server.Address;

public class AddressDAOTest {

	@Test
	public void test() {
		Address address = new AddressDAO().findAll().get(0);
		Assert.assertNotNull(address.getCountry());
		Assert.assertNotNull(address.getCounty());
		Assert.assertNotNull(address.getStreet_address());
		Assert.assertNotNull(address.getTown_village());
		Assert.assertNotNull(address.getZipcode());
	}

}
