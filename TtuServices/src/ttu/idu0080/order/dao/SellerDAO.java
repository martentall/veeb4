package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ttu.idu0080.order.server.Address;
import ttu.idu0080.order.server.Seller;

public class SellerDAO extends DAO<Seller>{

	@Override
	public String tablename() {
		return "enterprise";
	}
	
	public List<Seller> findAll() {
		List<Seller> o = new ArrayList<>();
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			
			try (ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM " + tablename() + " WHERE is_seller = 'Y'");) {
				while (resultSet.next()) {
					o.add(map(resultSet));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return o;
	}

	public Seller findById(Integer id) {
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			
			try (ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM " + tablename() + " WHERE " + tablename() + " = " + id + " AND is_seller = 'Y'");) {
				resultSet.next();
				return map(resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Seller map(ResultSet resultSet) throws SQLException {
		Seller seller = new Seller();
		seller.setEnterprise(resultSet.getInt("enterprise"));
		seller.setName(resultSet.getString("name"));
		return seller;	
	}

}
