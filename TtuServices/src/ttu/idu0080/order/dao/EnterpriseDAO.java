package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ttu.idu0080.order.server.Courier;
import ttu.idu0080.order.server.EntAddress;

public class EnterpriseDAO extends DAO<Courier> {

	@Override
	public String tablename() {
		return "enterprise";
	}

	public List<Courier> findAllCouriers() {
		List<Courier> o = new ArrayList<>();
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			try (ResultSet resultSet = 
					statement.executeQuery(
							"SELECT * FROM " + tablename() + " WHERE is_courier = 'Y'");) {
				while (resultSet.next()) {
					o.add(map(resultSet));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return o;
	}

	@Override
	public Courier map(ResultSet resultSet) throws SQLException {
		Courier courier = new Courier();
		courier.setEnterprise(resultSet.getInt("enterprise"));
		courier.setName(resultSet.getString("name"));
		courier.setPercent_from_order(resultSet.getInt("percent_from_order"));
		
		courier.setAddresses(loadAddresses(courier));
		return courier;
	}

	private EntAddress[] loadAddresses(Courier courier) {
		EntAddressDao entAddressDao = new EntAddressDao();
		List<EntAddress> addresses = entAddressDao.getEntAddressesByEntId(courier.getEnterprise());
		return addresses.toArray(new EntAddress[addresses.size()]);
	}
	
}
