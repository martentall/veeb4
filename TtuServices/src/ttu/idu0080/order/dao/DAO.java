package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class DAO<T> {

	public abstract String tablename();
	
	public abstract T map(ResultSet resultSet) throws SQLException;

	public List<T> findAll() {
		List<T> o = new ArrayList<>();
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			try (ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM " + tablename());) {
				while (resultSet.next()) {
					o.add(map(resultSet));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return o;
	}

	public T findById(Integer id) {
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			
			try (ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM " + tablename() + " WHERE " + tablename() + " = " + id);) {
				resultSet.next();
				return map(resultSet);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Connection connection() {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres","postgres","123456");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	@Deprecated
	public Statement statement() {
		try {
			return connection().createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
