package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ttu.idu0080.order.server.Order;

public class OrderDAO extends DAO<Order> {

	@Override
	public String tablename() {
		return "eshop_order";
	}

	public Order findById(Integer id) {
		try (Connection connection = connection();
				Statement statement = connection.createStatement()) {
			
			try (ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM eshop_order WHERE eshop_order = " + id);) {
				resultSet.next();
				return map(resultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Order map(ResultSet resultSet) throws SQLException {
		Order order = new Order();
		order.setOrder_id(resultSet.getInt("eshop_order"));
		order.setPrice_total(resultSet.getInt("price_total"));
		order.setShipping_address(new AddressDAO().findById(resultSet.getInt("shipping_address_fk")));
		order.setSeller(new SellerDAO().findById(resultSet.getInt("seller_fk")));
		return order;
	}
}
