package ttu.idu0080.order.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ttu.idu0080.order.server.EntAddress;

public class EntAddressDao extends DAO<EntAddress> {
	
	public List<EntAddress> getEntAddressesByEntId(Integer entId) {
		List<EntAddress> addresses = new ArrayList<>();
		try (Connection connection = connection();
				Statement statement = connection.createStatement();) {
			try(ResultSet resultSet 
					= statement.executeQuery(
							"SELECT * FROM ENT_ADDRESS WHERE subject_fk = " + entId);) {
				while(resultSet.next()) {
					addresses.add(map(resultSet));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return addresses;
	}
	@Override
	public String tablename() {
		return "ent_address";
	}

	@Override
	public EntAddress map(ResultSet resultSet) throws SQLException {
		EntAddress address = new EntAddress();
		address.setAddress(resultSet.getInt("ent_address"));
		address.setCountry(resultSet.getString("country"));
		address.setCounty(resultSet.getString("county"));
		address.setStreet_address(resultSet.getString("street_address"));
		address.setTown_village(resultSet.getString("town_village"));
		address.setZipcode(resultSet.getString("zipcode"));
		return address;
	}

}
