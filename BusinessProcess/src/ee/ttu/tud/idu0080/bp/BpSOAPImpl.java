/**
 * BpSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.bp;

import ttu.idu0080.order.server.program.BusinessProcessRunner;

public class BpSOAPImpl implements ee.ttu.tud.idu0080.bp.Bp_PortType{
    public MinuTellimuseVastus tellimuseEsitamine(ee.ttu.tud.idu0080.bp.MinuTellimus parameters) throws java.rmi.RemoteException {
        return new BusinessProcessRunner().run(parameters.getId().intValue());
    }
}
