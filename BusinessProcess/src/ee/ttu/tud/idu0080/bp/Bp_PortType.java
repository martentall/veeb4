/**
 * Bp_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.bp;

public interface Bp_PortType extends java.rmi.Remote {
    public ee.ttu.tud.idu0080.bp.MinuTellimuseVastus tellimuseEsitamine(ee.ttu.tud.idu0080.bp.MinuTellimus parameters) throws java.rmi.RemoteException;
}
