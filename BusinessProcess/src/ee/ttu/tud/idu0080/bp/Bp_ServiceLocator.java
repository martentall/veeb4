/**
 * Bp_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.bp;

public class Bp_ServiceLocator extends org.apache.axis.client.Service implements ee.ttu.tud.idu0080.bp.Bp_Service {

    public Bp_ServiceLocator() {
    }


    public Bp_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bp_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpSOAP
    private java.lang.String bpSOAP_address = "http://localhost:8085/BpService";

    public java.lang.String getbpSOAPAddress() {
        return bpSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpSOAPWSDDServiceName = "bpSOAP";

    public java.lang.String getbpSOAPWSDDServiceName() {
        return bpSOAPWSDDServiceName;
    }

    public void setbpSOAPWSDDServiceName(java.lang.String name) {
        bpSOAPWSDDServiceName = name;
    }

    public ee.ttu.tud.idu0080.bp.Bp_PortType getbpSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpSOAP(endpoint);
    }

    public ee.ttu.tud.idu0080.bp.Bp_PortType getbpSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ee.ttu.tud.idu0080.bp.BpSOAPStub _stub = new ee.ttu.tud.idu0080.bp.BpSOAPStub(portAddress, this);
            _stub.setPortName(getbpSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpSOAPEndpointAddress(java.lang.String address) {
        bpSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ee.ttu.tud.idu0080.bp.Bp_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ee.ttu.tud.idu0080.bp.BpSOAPStub _stub = new ee.ttu.tud.idu0080.bp.BpSOAPStub(new java.net.URL(bpSOAP_address), this);
                _stub.setPortName(getbpSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpSOAP".equals(inputPortName)) {
            return getbpSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", "bp");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", "bpSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpSOAP".equals(portName)) {
            setbpSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
