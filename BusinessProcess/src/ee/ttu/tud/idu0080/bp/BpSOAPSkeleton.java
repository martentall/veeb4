/**
 * BpSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.bp;

public class BpSOAPSkeleton implements ee.ttu.tud.idu0080.bp.Bp_PortType, org.apache.axis.wsdl.Skeleton {
    private ee.ttu.tud.idu0080.bp.Bp_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", "MinuTellimus"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", ">MinuTellimus"), ee.ttu.tud.idu0080.bp.MinuTellimus.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("tellimuseEsitamine", _params, new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", "MinuTellimuseVastus"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/bp/", ">MinuTellimuseVastus"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "TellimuseEsitamine"));
        _oper.setSoapAction("http://idu0080.tud.ttu.ee/bp/TellimuseEsitamine");
        _myOperationsList.add(_oper);
        if (_myOperations.get("tellimuseEsitamine") == null) {
            _myOperations.put("tellimuseEsitamine", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("tellimuseEsitamine")).add(_oper);
    }

    public BpSOAPSkeleton() {
        this.impl = new ee.ttu.tud.idu0080.bp.BpSOAPImpl();
    }

    public BpSOAPSkeleton(ee.ttu.tud.idu0080.bp.Bp_PortType impl) {
        this.impl = impl;
    }
    public ee.ttu.tud.idu0080.bp.MinuTellimuseVastus tellimuseEsitamine(ee.ttu.tud.idu0080.bp.MinuTellimus parameters) throws java.rmi.RemoteException
    {
        ee.ttu.tud.idu0080.bp.MinuTellimuseVastus ret = impl.tellimuseEsitamine(parameters);
        return ret;
    }

}
