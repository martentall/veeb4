/**
 * Transport_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.transport;

public interface Transport_PortType extends java.rmi.Remote {
    public java.lang.String transport(java.lang.String pakkumus_id) throws java.rmi.RemoteException;
}
