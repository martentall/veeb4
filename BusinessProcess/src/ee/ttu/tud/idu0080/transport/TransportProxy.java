package ee.ttu.tud.idu0080.transport;

public class TransportProxy implements ee.ttu.tud.idu0080.transport.Transport_PortType {
  private String _endpoint = null;
  private ee.ttu.tud.idu0080.transport.Transport_PortType transport_PortType = null;
  
  public TransportProxy() {
    _initTransportProxy();
  }
  
  public TransportProxy(String endpoint) {
    _endpoint = endpoint;
    _initTransportProxy();
  }
  
  private void _initTransportProxy() {
    try {
      transport_PortType = (new ee.ttu.tud.idu0080.transport.Transport_ServiceLocator()).gettransportSOAP();
      if (transport_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)transport_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)transport_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (transport_PortType != null)
      ((javax.xml.rpc.Stub)transport_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ee.ttu.tud.idu0080.transport.Transport_PortType getTransport_PortType() {
    if (transport_PortType == null)
      _initTransportProxy();
    return transport_PortType;
  }
  
  public java.lang.String transport(java.lang.String pakkumus_id) throws java.rmi.RemoteException{
    if (transport_PortType == null)
      _initTransportProxy();
    return transport_PortType.transport(pakkumus_id);
  }
  
  
}