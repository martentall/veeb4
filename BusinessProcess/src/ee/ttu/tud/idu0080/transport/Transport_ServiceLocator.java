/**
 * Transport_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.transport;

public class Transport_ServiceLocator extends org.apache.axis.client.Service implements ee.ttu.tud.idu0080.transport.Transport_Service {

    public Transport_ServiceLocator() {
    }


    public Transport_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Transport_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for transportSOAP
    private java.lang.String transportSOAP_address = "http://localhost:8080/CouriersService/services/transportSOAP";

    public java.lang.String gettransportSOAPAddress() {
        return transportSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String transportSOAPWSDDServiceName = "transportSOAP";

    public java.lang.String gettransportSOAPWSDDServiceName() {
        return transportSOAPWSDDServiceName;
    }

    public void settransportSOAPWSDDServiceName(java.lang.String name) {
        transportSOAPWSDDServiceName = name;
    }

    public ee.ttu.tud.idu0080.transport.Transport_PortType gettransportSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(transportSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return gettransportSOAP(endpoint);
    }

    public ee.ttu.tud.idu0080.transport.Transport_PortType gettransportSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ee.ttu.tud.idu0080.transport.TransportSOAPStub _stub = new ee.ttu.tud.idu0080.transport.TransportSOAPStub(portAddress, this);
            _stub.setPortName(gettransportSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void settransportSOAPEndpointAddress(java.lang.String address) {
        transportSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ee.ttu.tud.idu0080.transport.Transport_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ee.ttu.tud.idu0080.transport.TransportSOAPStub _stub = new ee.ttu.tud.idu0080.transport.TransportSOAPStub(new java.net.URL(transportSOAP_address), this);
                _stub.setPortName(gettransportSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("transportSOAP".equals(inputPortName)) {
            return gettransportSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/transport/", "transport");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/transport/", "transportSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("transportSOAP".equals(portName)) {
            settransportSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
