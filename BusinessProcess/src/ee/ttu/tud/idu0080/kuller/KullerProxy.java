package ee.ttu.tud.idu0080.kuller;

public class KullerProxy implements ee.ttu.tud.idu0080.kuller.Kuller_PortType {
  private String _endpoint = null;
  private ee.ttu.tud.idu0080.kuller.Kuller_PortType kuller_PortType = null;
  
  public KullerProxy() {
    _initKullerProxy();
  }
  
  public KullerProxy(String endpoint) {
    _endpoint = endpoint;
    _initKullerProxy();
  }
  
  private void _initKullerProxy() {
    try {
      kuller_PortType = (new ee.ttu.tud.idu0080.kuller.Kuller_ServiceLocator()).getkullerSOAP();
      if (kuller_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)kuller_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)kuller_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (kuller_PortType != null)
      ((javax.xml.rpc.Stub)kuller_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ee.ttu.tud.idu0080.kuller.Kuller_PortType getKuller_PortType() {
    if (kuller_PortType == null)
      _initKullerProxy();
    return kuller_PortType;
  }
  
  public ee.ttu.tud.idu0080.kuller.Kuller_Type[] getKullerid() throws java.rmi.RemoteException{
    if (kuller_PortType == null)
      _initKullerProxy();
    return kuller_PortType.getKullerid();
  }
  
  
}