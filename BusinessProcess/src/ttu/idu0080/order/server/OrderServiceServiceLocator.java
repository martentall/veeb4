/**
 * OrderServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class OrderServiceServiceLocator extends org.apache.axis.client.Service implements ttu.idu0080.order.server.OrderServiceService {

    public OrderServiceServiceLocator() {
    }


    public OrderServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public OrderServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for OrderServicePort
    private java.lang.String OrderServicePort_address = "http://localhost:8080/TtuServices/services/OrderServicePort";

    public java.lang.String getOrderServicePortAddress() {
        return OrderServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String OrderServicePortWSDDServiceName = "OrderServicePort";

    public java.lang.String getOrderServicePortWSDDServiceName() {
        return OrderServicePortWSDDServiceName;
    }

    public void setOrderServicePortWSDDServiceName(java.lang.String name) {
        OrderServicePortWSDDServiceName = name;
    }

    public ttu.idu0080.order.server.OrderService getOrderServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(OrderServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getOrderServicePort(endpoint);
    }

    public ttu.idu0080.order.server.OrderService getOrderServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ttu.idu0080.order.server.OrderServicePortBindingStub _stub = new ttu.idu0080.order.server.OrderServicePortBindingStub(portAddress, this);
            _stub.setPortName(getOrderServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setOrderServicePortEndpointAddress(java.lang.String address) {
        OrderServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ttu.idu0080.order.server.OrderService.class.isAssignableFrom(serviceEndpointInterface)) {
                ttu.idu0080.order.server.OrderServicePortBindingStub _stub = new ttu.idu0080.order.server.OrderServicePortBindingStub(new java.net.URL(OrderServicePort_address), this);
                _stub.setPortName(getOrderServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("OrderServicePort".equals(inputPortName)) {
            return getOrderServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "OrderServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "OrderServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("OrderServicePort".equals(portName)) {
            setOrderServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
