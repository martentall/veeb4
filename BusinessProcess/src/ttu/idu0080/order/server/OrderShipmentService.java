/**
 * OrderShipmentService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public interface OrderShipmentService extends java.rmi.Remote {
    public ttu.idu0080.order.server.OrderShipment[] getOrderShipmentsByOrderIdRyhmName(int order_id, java.lang.String ryhm_name) throws java.rmi.RemoteException;
    public int insertOrderShipment(int order_id, java.lang.String ryhm_name, java.lang.String tracking_number, java.lang.String courier_name, java.util.Calendar approx_delivery_date, double shipping_price) throws java.rmi.RemoteException;
}
