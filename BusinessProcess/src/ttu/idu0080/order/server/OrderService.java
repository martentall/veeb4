/**
 * OrderService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public interface OrderService extends java.rmi.Remote {
    public ttu.idu0080.order.server.Order[] getOrdersByCustomerId(int customer_id) throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Order[] getOrdersByShippingAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Order[] getOrdersByCustomerLastName(java.lang.String last_name) throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Order[] getAllOrders() throws java.rmi.RemoteException;
    public ttu.idu0080.order.server.Order getOrdersByOrderId(int order_id) throws java.rmi.RemoteException;
}
