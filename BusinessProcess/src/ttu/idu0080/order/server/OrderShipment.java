/**
 * OrderShipment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class OrderShipment  implements java.io.Serializable {
    private java.util.Calendar approx_delivery_date;

    private java.lang.String courier_name;

    private int order;

    private int order_shipment;

    private java.lang.String ryhm_name;

    private double shipping_price;

    private java.lang.String tracking_number;

    public OrderShipment() {
    }

    public OrderShipment(
           java.util.Calendar approx_delivery_date,
           java.lang.String courier_name,
           int order,
           int order_shipment,
           java.lang.String ryhm_name,
           double shipping_price,
           java.lang.String tracking_number) {
           this.approx_delivery_date = approx_delivery_date;
           this.courier_name = courier_name;
           this.order = order;
           this.order_shipment = order_shipment;
           this.ryhm_name = ryhm_name;
           this.shipping_price = shipping_price;
           this.tracking_number = tracking_number;
    }


    /**
     * Gets the approx_delivery_date value for this OrderShipment.
     * 
     * @return approx_delivery_date
     */
    public java.util.Calendar getApprox_delivery_date() {
        return approx_delivery_date;
    }


    /**
     * Sets the approx_delivery_date value for this OrderShipment.
     * 
     * @param approx_delivery_date
     */
    public void setApprox_delivery_date(java.util.Calendar approx_delivery_date) {
        this.approx_delivery_date = approx_delivery_date;
    }


    /**
     * Gets the courier_name value for this OrderShipment.
     * 
     * @return courier_name
     */
    public java.lang.String getCourier_name() {
        return courier_name;
    }


    /**
     * Sets the courier_name value for this OrderShipment.
     * 
     * @param courier_name
     */
    public void setCourier_name(java.lang.String courier_name) {
        this.courier_name = courier_name;
    }


    /**
     * Gets the order value for this OrderShipment.
     * 
     * @return order
     */
    public int getOrder() {
        return order;
    }


    /**
     * Sets the order value for this OrderShipment.
     * 
     * @param order
     */
    public void setOrder(int order) {
        this.order = order;
    }


    /**
     * Gets the order_shipment value for this OrderShipment.
     * 
     * @return order_shipment
     */
    public int getOrder_shipment() {
        return order_shipment;
    }


    /**
     * Sets the order_shipment value for this OrderShipment.
     * 
     * @param order_shipment
     */
    public void setOrder_shipment(int order_shipment) {
        this.order_shipment = order_shipment;
    }


    /**
     * Gets the ryhm_name value for this OrderShipment.
     * 
     * @return ryhm_name
     */
    public java.lang.String getRyhm_name() {
        return ryhm_name;
    }


    /**
     * Sets the ryhm_name value for this OrderShipment.
     * 
     * @param ryhm_name
     */
    public void setRyhm_name(java.lang.String ryhm_name) {
        this.ryhm_name = ryhm_name;
    }


    /**
     * Gets the shipping_price value for this OrderShipment.
     * 
     * @return shipping_price
     */
    public double getShipping_price() {
        return shipping_price;
    }


    /**
     * Sets the shipping_price value for this OrderShipment.
     * 
     * @param shipping_price
     */
    public void setShipping_price(double shipping_price) {
        this.shipping_price = shipping_price;
    }


    /**
     * Gets the tracking_number value for this OrderShipment.
     * 
     * @return tracking_number
     */
    public java.lang.String getTracking_number() {
        return tracking_number;
    }


    /**
     * Sets the tracking_number value for this OrderShipment.
     * 
     * @param tracking_number
     */
    public void setTracking_number(java.lang.String tracking_number) {
        this.tracking_number = tracking_number;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderShipment)) return false;
        OrderShipment other = (OrderShipment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.approx_delivery_date==null && other.getApprox_delivery_date()==null) || 
             (this.approx_delivery_date!=null &&
              this.approx_delivery_date.equals(other.getApprox_delivery_date()))) &&
            ((this.courier_name==null && other.getCourier_name()==null) || 
             (this.courier_name!=null &&
              this.courier_name.equals(other.getCourier_name()))) &&
            this.order == other.getOrder() &&
            this.order_shipment == other.getOrder_shipment() &&
            ((this.ryhm_name==null && other.getRyhm_name()==null) || 
             (this.ryhm_name!=null &&
              this.ryhm_name.equals(other.getRyhm_name()))) &&
            this.shipping_price == other.getShipping_price() &&
            ((this.tracking_number==null && other.getTracking_number()==null) || 
             (this.tracking_number!=null &&
              this.tracking_number.equals(other.getTracking_number())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApprox_delivery_date() != null) {
            _hashCode += getApprox_delivery_date().hashCode();
        }
        if (getCourier_name() != null) {
            _hashCode += getCourier_name().hashCode();
        }
        _hashCode += getOrder();
        _hashCode += getOrder_shipment();
        if (getRyhm_name() != null) {
            _hashCode += getRyhm_name().hashCode();
        }
        _hashCode += new Double(getShipping_price()).hashCode();
        if (getTracking_number() != null) {
            _hashCode += getTracking_number().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderShipment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "orderShipment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approx_delivery_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "approx_delivery_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("courier_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "courier_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_shipment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_shipment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ryhm_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ryhm_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tracking_number");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tracking_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
