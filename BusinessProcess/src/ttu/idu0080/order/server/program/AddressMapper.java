package ttu.idu0080.order.server.program;

import ttu.idu0080.order.server.Address;
import ttu.idu0080.order.server.EntAddress;
import ttu.idu0080.order.server.Seller;

public class AddressMapper {
	
	public org.example.www.pakkumus.Address mapShippingAddress(
			Address shipping_address) {
		org.example.www.pakkumus.Address pakkumusAddress = new org.example.www.pakkumus.Address();
		pakkumusAddress.setAddress(shipping_address.getAddress());
		pakkumusAddress.setCountry(shipping_address.getCounty());
		pakkumusAddress.setCounty(shipping_address.getCounty());
		pakkumusAddress.setStreet_address(shipping_address.getStreet_address());
		pakkumusAddress.setTown_village(shipping_address.getTown_village());
		pakkumusAddress.setZipcode(shipping_address.getTown_village());
		return pakkumusAddress;
	}

	public org.example.www.pakkumus.Address[] mapSellerAddresses(Seller seller) {
		if (seller.getAddresses() == null) {
			return new org.example.www.pakkumus.Address[0];
		}
		
		org.example.www.pakkumus.Address[] addresses = 
				new org.example.www.pakkumus.Address[seller.getAddresses().length];
		for (int i = 0; i < seller.getAddresses().length; i++) {
			addresses[i] = mapAddress(seller.getAddresses()[i]);
		}
		return addresses;
	}

	public org.example.www.pakkumus.Address mapAddress(
			EntAddress entAddress) {
		org.example.www.pakkumus.Address sellerAddress = new org.example.www.pakkumus.Address();
		sellerAddress.setAddress(entAddress.getAddress());
		sellerAddress.setCountry(entAddress.getCounty());
		sellerAddress.setCounty(entAddress.getCounty());
		sellerAddress.setStreet_address(entAddress.getStreet_address());
		sellerAddress.setTown_village(entAddress.getTown_village());
		sellerAddress.setZipcode(entAddress.getTown_village());
		return sellerAddress;
	}
}