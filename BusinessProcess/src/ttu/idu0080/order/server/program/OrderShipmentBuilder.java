package ttu.idu0080.order.server.program;

import java.time.LocalDate;
import java.util.Calendar;

import ttu.idu0080.order.server.Order;
import ttu.idu0080.order.server.OrderShipment;

public class OrderShipmentBuilder {

	private static final String RYHM_458 = "RYHM_458";

	public static OrderShipment build(Order order, Pakkumus bestPakkumus, String shipment_code) {
		OrderShipment shipment = new OrderShipment();
		shipment.setOrder(order.getOrder_id());
		shipment.setCourier_name(bestPakkumus.kuller.getNimi());
		shipment.setRyhm_name(RYHM_458);
		shipment.setShipping_price(bestPakkumus.vastus.getHind());
		shipment.setTracking_number(shipment_code);
		shipment.setApprox_delivery_date(getShipmentTime(bestPakkumus));
		return shipment;
	}

	private static Calendar getShipmentTime(Pakkumus bestPakkumus) {
		LocalDate shipmentDate = LocalDate.now().plusDays(bestPakkumus.vastus.getPaevade_arv());
		Calendar cal = Calendar.getInstance();
		cal.set(shipmentDate.getYear(), shipmentDate.getMonthValue()-1, shipmentDate.getDayOfMonth());
		return cal;
	}
}
