package ttu.idu0080.order.server.program;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.example.www.pakkumus.PakkumusParing;
import org.example.www.pakkumus.PakkumusProxy;
import org.example.www.pakkumus.PakkumusVastus;

import ttu.idu0080.order.server.Order;
import ttu.idu0080.order.server.OrderServiceProxy;
import ttu.idu0080.order.server.OrderShipment;
import ttu.idu0080.order.server.OrderShipmentServiceProxy;
import ee.ttu.tud.idu0080.bp.MinuTellimuseVastus;
import ee.ttu.tud.idu0080.kuller.KullerProxy;
import ee.ttu.tud.idu0080.kuller.Kuller_Type;
import ee.ttu.tud.idu0080.transport.TransportProxy;

public class BusinessProcessRunner {
	private KullerProxy kullerProxy;
	private TransportProxy transportProxy;
	private PakkumusProxy pakkumusProxy;
	private OrderServiceProxy orderProxy;
	private OrderShipmentServiceProxy shipmentProxy;
	
	public BusinessProcessRunner() {
		this.kullerProxy = new KullerProxy();
		this.transportProxy = new TransportProxy();
		this.pakkumusProxy = new PakkumusProxy();
		this.orderProxy = new OrderServiceProxy();
		this.shipmentProxy = new OrderShipmentServiceProxy();
	}
	
	public MinuTellimuseVastus run(int tellimusId) {
		try {
			return createMinuTellimusVastus(tellimusId);
		} catch(Exception e) {
			e.printStackTrace();
			return createMinuTellimusVastusOnError();
		}
	}

	private MinuTellimuseVastus createMinuTellimusVastusOnError() {
		return new MinuTellimuseVastus("result: exception");
	}

	private MinuTellimuseVastus createMinuTellimusVastus(int tellimusId)
			throws RemoteException {
		Order order = orderProxy.getOrdersByOrderId(tellimusId);
		Kuller_Type[] kullerid = kullerProxy.getKullerid();
		
		List<Pakkumus> pakkumused = getPakkumused(order, kullerid);
		Pakkumus bestPakkumus = getBestPakkumus(pakkumused);
		
		String shipment_code = transportProxy.transport(bestPakkumus.vastus.getOffer_id());
		
		int result = insertOrderShipment(order, bestPakkumus, shipment_code);
		return new MinuTellimuseVastus("result: " + result);
	}

	private int insertOrderShipment(Order order, Pakkumus bestPakkumus,
			String shipment_code) throws RemoteException {
		OrderShipment orderShipment = OrderShipmentBuilder.build(order, bestPakkumus, shipment_code);
		int result = shipmentProxy.insertOrderShipment(
				orderShipment.getOrder(),
				orderShipment.getRyhm_name(), 
				orderShipment.getTracking_number(),
				orderShipment.getCourier_name(),
				orderShipment.getApprox_delivery_date(),
				orderShipment.getShipping_price());
		return result;
	}

	private Pakkumus getBestPakkumus(List<Pakkumus> pakkumused) {
		Collections.sort(pakkumused, 
				(Pakkumus p1, Pakkumus p2) -> 
					PakkumusCalculator.calculate(p1).compareTo(PakkumusCalculator.calculate(p2)));
		return pakkumused.get(0);
	}

	private List<Pakkumus> getPakkumused(Order order, Kuller_Type[] kullerid)
			throws RemoteException {
		List<Pakkumus> pakkumused = new ArrayList<>();
		for (Kuller_Type kuller_Type : kullerid) {
			PakkumusParing pakkumusParing = new PakkumusParingCreator().create(kuller_Type, order);
			PakkumusVastus pakkumusVastus = pakkumusProxy.teePakkumus(pakkumusParing);
			pakkumused.add(new Pakkumus(pakkumusParing, pakkumusVastus, kuller_Type));
		}
		return pakkumused;
	}
}
