package ttu.idu0080.order.server.program;

import org.example.www.pakkumus.PakkumusParing;
import org.example.www.pakkumus.TellimusType;

import ttu.idu0080.order.server.Order;
import ttu.idu0080.order.server.Seller;
import ee.ttu.tud.idu0080.kuller.Kuller_Type;

public class PakkumusParingCreator {
		
		public PakkumusParing create(Kuller_Type kuller, Order order) {
			PakkumusParing pakkumusParing = new PakkumusParing();
			pakkumusParing.setKuller_id(kuller.getKuller_id());
			pakkumusParing.setTellimus(mapTellimus(order));
			return pakkumusParing;
		}

		private TellimusType mapTellimus(Order order) {
			TellimusType tellimus = new TellimusType();
			tellimus.setOrder_id(order.getOrder_id());
			tellimus.setPrice_total(order.getPrice_total());
			tellimus.setSeller(mapSeller(order.getSeller()));
			tellimus.setShipping_address(new AddressMapper().mapShippingAddress(order.getShipping_address()));
			return tellimus;
		}

		private org.example.www.pakkumus.Seller mapSeller(Seller seller) {
			org.example.www.pakkumus.Seller pakkumusSeller = new org.example.www.pakkumus.Seller();
			pakkumusSeller.setEnterprise(seller.getEnterprise());
			pakkumusSeller.setName(seller.getName());
			pakkumusSeller.setAddresses(new AddressMapper().mapSellerAddresses(seller));
			return pakkumusSeller;
		}
	}