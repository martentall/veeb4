package ttu.idu0080.order.server.program;

import org.example.www.pakkumus.PakkumusParing;
import org.example.www.pakkumus.PakkumusVastus;

import ee.ttu.tud.idu0080.kuller.Kuller_Type;

public class Pakkumus {
	public final PakkumusParing paring;
	public final PakkumusVastus vastus;
	public final Kuller_Type kuller;
	
	public Pakkumus(PakkumusParing paring, PakkumusVastus vastus, Kuller_Type kuller) {
		this.paring = paring;
		this.vastus = vastus;
		this.kuller = kuller;
	}
}
