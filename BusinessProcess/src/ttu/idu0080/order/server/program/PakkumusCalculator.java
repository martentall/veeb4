package ttu.idu0080.order.server.program;

public class PakkumusCalculator {

	public static Double calculate(Pakkumus pakkumus) {
		return pakkumus.vastus.getHind() / 100 * pakkumus.vastus.getPaevade_arv(); 
	}
}
