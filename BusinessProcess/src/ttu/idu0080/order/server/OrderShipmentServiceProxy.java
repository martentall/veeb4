package ttu.idu0080.order.server;

public class OrderShipmentServiceProxy implements ttu.idu0080.order.server.OrderShipmentService {
  private String _endpoint = null;
  private ttu.idu0080.order.server.OrderShipmentService orderShipmentService = null;
  
  public OrderShipmentServiceProxy() {
    _initOrderShipmentServiceProxy();
  }
  
  public OrderShipmentServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initOrderShipmentServiceProxy();
  }
  
  private void _initOrderShipmentServiceProxy() {
    try {
      orderShipmentService = (new ttu.idu0080.order.server.OrderShipmentServiceServiceLocator()).getOrderShipmentServicePort();
      if (orderShipmentService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)orderShipmentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)orderShipmentService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (orderShipmentService != null)
      ((javax.xml.rpc.Stub)orderShipmentService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ttu.idu0080.order.server.OrderShipmentService getOrderShipmentService() {
    if (orderShipmentService == null)
      _initOrderShipmentServiceProxy();
    return orderShipmentService;
  }
  
  public ttu.idu0080.order.server.OrderShipment[] getOrderShipmentsByOrderIdRyhmName(int order_id, java.lang.String ryhm_name) throws java.rmi.RemoteException{
    if (orderShipmentService == null)
      _initOrderShipmentServiceProxy();
    return orderShipmentService.getOrderShipmentsByOrderIdRyhmName(order_id, ryhm_name);
  }
  
  public int insertOrderShipment(int order_id, java.lang.String ryhm_name, java.lang.String tracking_number, java.lang.String courier_name, java.util.Calendar approx_delivery_date, double shipping_price) throws java.rmi.RemoteException{
    if (orderShipmentService == null)
      _initOrderShipmentServiceProxy();
    return orderShipmentService.insertOrderShipment(order_id, ryhm_name, tracking_number, courier_name, approx_delivery_date, shipping_price);
  }
  
  
}