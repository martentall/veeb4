package ttu.idu0080.order.server;

public class OrderServiceProxy implements ttu.idu0080.order.server.OrderService {
  private String _endpoint = null;
  private ttu.idu0080.order.server.OrderService orderService = null;
  
  public OrderServiceProxy() {
    _initOrderServiceProxy();
  }
  
  public OrderServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initOrderServiceProxy();
  }
  
  private void _initOrderServiceProxy() {
    try {
      orderService = (new ttu.idu0080.order.server.OrderServiceServiceLocator()).getOrderServicePort();
      if (orderService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)orderService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)orderService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (orderService != null)
      ((javax.xml.rpc.Stub)orderService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ttu.idu0080.order.server.OrderService getOrderService() {
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService;
  }
  
  public ttu.idu0080.order.server.Order[] getOrdersByCustomerId(int customer_id) throws java.rmi.RemoteException{
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService.getOrdersByCustomerId(customer_id);
  }
  
  public ttu.idu0080.order.server.Order[] getOrdersByShippingAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException{
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService.getOrdersByShippingAddress(country, county);
  }
  
  public ttu.idu0080.order.server.Order[] getOrdersByCustomerLastName(java.lang.String last_name) throws java.rmi.RemoteException{
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService.getOrdersByCustomerLastName(last_name);
  }
  
  public ttu.idu0080.order.server.Order[] getAllOrders() throws java.rmi.RemoteException{
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService.getAllOrders();
  }
  
  public ttu.idu0080.order.server.Order getOrdersByOrderId(int order_id) throws java.rmi.RemoteException{
    if (orderService == null)
      _initOrderServiceProxy();
    return orderService.getOrdersByOrderId(order_id);
  }
  
  
}