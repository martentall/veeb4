/**
 * PakkumusVastus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public class PakkumusVastus  implements java.io.Serializable {
    private double hind;

    private java.lang.Integer paevade_arv;

    private java.lang.String offer_id;

    public PakkumusVastus() {
    }

    public PakkumusVastus(
           double hind,
           java.lang.Integer paevade_arv,
           java.lang.String offer_id) {
           this.hind = hind;
           this.paevade_arv = paevade_arv;
           this.offer_id = offer_id;
    }


    /**
     * Gets the hind value for this PakkumusVastus.
     * 
     * @return hind
     */
    public double getHind() {
        return hind;
    }


    /**
     * Sets the hind value for this PakkumusVastus.
     * 
     * @param hind
     */
    public void setHind(double hind) {
        this.hind = hind;
    }


    /**
     * Gets the paevade_arv value for this PakkumusVastus.
     * 
     * @return paevade_arv
     */
    public java.lang.Integer getPaevade_arv() {
        return paevade_arv;
    }


    /**
     * Sets the paevade_arv value for this PakkumusVastus.
     * 
     * @param paevade_arv
     */
    public void setPaevade_arv(java.lang.Integer paevade_arv) {
        this.paevade_arv = paevade_arv;
    }


    /**
     * Gets the offer_id value for this PakkumusVastus.
     * 
     * @return offer_id
     */
    public java.lang.String getOffer_id() {
        return offer_id;
    }


    /**
     * Sets the offer_id value for this PakkumusVastus.
     * 
     * @param offer_id
     */
    public void setOffer_id(java.lang.String offer_id) {
        this.offer_id = offer_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PakkumusVastus)) return false;
        PakkumusVastus other = (PakkumusVastus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.hind == other.getHind() &&
            ((this.paevade_arv==null && other.getPaevade_arv()==null) || 
             (this.paevade_arv!=null &&
              this.paevade_arv.equals(other.getPaevade_arv()))) &&
            ((this.offer_id==null && other.getOffer_id()==null) || 
             (this.offer_id!=null &&
              this.offer_id.equals(other.getOffer_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Double(getHind()).hashCode();
        if (getPaevade_arv() != null) {
            _hashCode += getPaevade_arv().hashCode();
        }
        if (getOffer_id() != null) {
            _hashCode += getOffer_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PakkumusVastus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", ">PakkumusVastus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hind");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hind"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paevade_arv");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paevade_arv"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offer_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "offer_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
