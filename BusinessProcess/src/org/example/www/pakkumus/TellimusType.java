/**
 * TellimusType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public class TellimusType  implements java.io.Serializable {
    private int order_id;

    private double price_total;

    private org.example.www.pakkumus.Seller seller;

    private org.example.www.pakkumus.Address shipping_address;

    public TellimusType() {
    }

    public TellimusType(
           int order_id,
           double price_total,
           org.example.www.pakkumus.Seller seller,
           org.example.www.pakkumus.Address shipping_address) {
           this.order_id = order_id;
           this.price_total = price_total;
           this.seller = seller;
           this.shipping_address = shipping_address;
    }


    /**
     * Gets the order_id value for this TellimusType.
     * 
     * @return order_id
     */
    public int getOrder_id() {
        return order_id;
    }


    /**
     * Sets the order_id value for this TellimusType.
     * 
     * @param order_id
     */
    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }


    /**
     * Gets the price_total value for this TellimusType.
     * 
     * @return price_total
     */
    public double getPrice_total() {
        return price_total;
    }


    /**
     * Sets the price_total value for this TellimusType.
     * 
     * @param price_total
     */
    public void setPrice_total(double price_total) {
        this.price_total = price_total;
    }


    /**
     * Gets the seller value for this TellimusType.
     * 
     * @return seller
     */
    public org.example.www.pakkumus.Seller getSeller() {
        return seller;
    }


    /**
     * Sets the seller value for this TellimusType.
     * 
     * @param seller
     */
    public void setSeller(org.example.www.pakkumus.Seller seller) {
        this.seller = seller;
    }


    /**
     * Gets the shipping_address value for this TellimusType.
     * 
     * @return shipping_address
     */
    public org.example.www.pakkumus.Address getShipping_address() {
        return shipping_address;
    }


    /**
     * Sets the shipping_address value for this TellimusType.
     * 
     * @param shipping_address
     */
    public void setShipping_address(org.example.www.pakkumus.Address shipping_address) {
        this.shipping_address = shipping_address;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TellimusType)) return false;
        TellimusType other = (TellimusType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.order_id == other.getOrder_id() &&
            this.price_total == other.getPrice_total() &&
            ((this.seller==null && other.getSeller()==null) || 
             (this.seller!=null &&
              this.seller.equals(other.getSeller()))) &&
            ((this.shipping_address==null && other.getShipping_address()==null) || 
             (this.shipping_address!=null &&
              this.shipping_address.equals(other.getShipping_address())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOrder_id();
        _hashCode += new Double(getPrice_total()).hashCode();
        if (getSeller() != null) {
            _hashCode += getSeller().hashCode();
        }
        if (getShipping_address() != null) {
            _hashCode += getShipping_address().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TellimusType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "tellimusType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("seller");
        elemField.setXmlName(new javax.xml.namespace.QName("", "seller"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "seller"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping_address");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "address"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
