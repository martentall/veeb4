package org.example.www.pakkumus;

public class PakkumusProxy implements org.example.www.pakkumus.Pakkumus_PortType {
  private String _endpoint = null;
  private org.example.www.pakkumus.Pakkumus_PortType pakkumus_PortType = null;
  
  public PakkumusProxy() {
    _initPakkumusProxy();
  }
  
  public PakkumusProxy(String endpoint) {
    _endpoint = endpoint;
    _initPakkumusProxy();
  }
  
  private void _initPakkumusProxy() {
    try {
      pakkumus_PortType = (new org.example.www.pakkumus.Pakkumus_ServiceLocator()).getpakkumusSOAP();
      if (pakkumus_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)pakkumus_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)pakkumus_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (pakkumus_PortType != null)
      ((javax.xml.rpc.Stub)pakkumus_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.example.www.pakkumus.Pakkumus_PortType getPakkumus_PortType() {
    if (pakkumus_PortType == null)
      _initPakkumusProxy();
    return pakkumus_PortType;
  }
  
  public org.example.www.pakkumus.PakkumusVastus teePakkumus(org.example.www.pakkumus.PakkumusParing parameters) throws java.rmi.RemoteException{
    if (pakkumus_PortType == null)
      _initPakkumusProxy();
    return pakkumus_PortType.teePakkumus(parameters);
  }
  
  
}