/**
 * Pakkumus_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public interface Pakkumus_PortType extends java.rmi.Remote {
    public org.example.www.pakkumus.PakkumusVastus teePakkumus(org.example.www.pakkumus.PakkumusParing parameters) throws java.rmi.RemoteException;
}
