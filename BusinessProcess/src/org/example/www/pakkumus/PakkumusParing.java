/**
 * PakkumusParing.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public class PakkumusParing  implements java.io.Serializable {
    private int kuller_id;

    private org.example.www.pakkumus.TellimusType tellimus;

    public PakkumusParing() {
    }

    public PakkumusParing(
           int kuller_id,
           org.example.www.pakkumus.TellimusType tellimus) {
           this.kuller_id = kuller_id;
           this.tellimus = tellimus;
    }


    /**
     * Gets the kuller_id value for this PakkumusParing.
     * 
     * @return kuller_id
     */
    public int getKuller_id() {
        return kuller_id;
    }


    /**
     * Sets the kuller_id value for this PakkumusParing.
     * 
     * @param kuller_id
     */
    public void setKuller_id(int kuller_id) {
        this.kuller_id = kuller_id;
    }


    /**
     * Gets the tellimus value for this PakkumusParing.
     * 
     * @return tellimus
     */
    public org.example.www.pakkumus.TellimusType getTellimus() {
        return tellimus;
    }


    /**
     * Sets the tellimus value for this PakkumusParing.
     * 
     * @param tellimus
     */
    public void setTellimus(org.example.www.pakkumus.TellimusType tellimus) {
        this.tellimus = tellimus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PakkumusParing)) return false;
        PakkumusParing other = (PakkumusParing) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.kuller_id == other.getKuller_id() &&
            ((this.tellimus==null && other.getTellimus()==null) || 
             (this.tellimus!=null &&
              this.tellimus.equals(other.getTellimus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getKuller_id();
        if (getTellimus() != null) {
            _hashCode += getTellimus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PakkumusParing.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", ">PakkumusParing"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kuller_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "kuller_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tellimus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tellimus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "tellimusType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
