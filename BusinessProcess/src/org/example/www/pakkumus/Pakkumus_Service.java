/**
 * Pakkumus_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public interface Pakkumus_Service extends javax.xml.rpc.Service {
    public java.lang.String getpakkumusSOAPAddress();

    public org.example.www.pakkumus.Pakkumus_PortType getpakkumusSOAP() throws javax.xml.rpc.ServiceException;

    public org.example.www.pakkumus.Pakkumus_PortType getpakkumusSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
