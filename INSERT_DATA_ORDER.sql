
INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Juhan','Juurikas','54637474','11.11.1967',NOW());

INSERT INTO address (subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (1,1,'Eesti','Harju','Tallinn','Liivalaia 34A-11','5262633');

INSERT INTO address (subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (1,1,'Suurbritannia','Sussex','Lewes','Fish Road 34A-11','5262633');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Marten','Maasikas','672727337XX','11.11.1977',NOW());

INSERT INTO address (subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (2,1,'Eesti','Tartu','Tartu','Kuuse 111','434443');
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (2,1,'Rootsi','Stockholm','Vaxholm','Gustav Vasa 45','5647474');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Marten','Maasikas','672727337XX','11.11.1977',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (3,1,'Eesti','Harju','Tallinn','Tartu mnt. 67','123333');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Tanel','Tuisk','672727337XX','11.11.1980',NOW());

INSERT INTO address (subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (4,1,'Eesti','Valgamaa','Valga','Tamme 55','1111');
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (4,1,'Eesti','Parnu','Parnu','Tuule 5','6677272');


INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Tambet','Kalapea','672727337XX','11.11.1980',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (5,1,'Eesti','Harju','Tallinn','Narva mnt. 67','4545656');

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (5,1,'Eesti','Valgamaa','Valga','Traktori 55','78382');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Vahur','Vaikne','672727337XX','11.11.1980',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (6,1,'Eesti','Parnu','Parnu','Kuuse 5','6677272');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Kaarel','Klient','5555555555XXXX','11.11.1970',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (7,1,'Eesti','Harju','Tallinn','Akadeemia 21','6663363');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Alexander','Carlsson','5555555555XXXX','11.11.1970',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (8,1,'Norra','Oslo','Oslo','Lennuki 55','78382');
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (8,1,'Eesti','Parnu','Parnu','Mereranna 95','1112222');


INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Anna','Aru','57838222','11.11.1975',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (9,1,'Eesti','Harju','Tallinn','Akadeemia 21','6663363');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Anna','Aru','57838222','11.11.1975',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (10,1,'Taani','Arhus','Arhus','Rongi 55','78382');
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (10,1,'Rootsi','Norbotten','Boden','Vasa 95','1112222');

INSERT INTO customer (first_name, last_name, identity_code, birth_date, created) VALUES ('Katariina','Suur','57838222','11.11.1975',NOW());

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (11,1,'Taani','Arhus','Arhus','Jokkmokk 89','78382');
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (11,1,'Rootsi','Norbotten','Pajala','Vasa 95','1112222');

INSERT INTO customer(first_name, last_name, identity_code, birth_date, created) VALUES ('Tauno','Toru','672727337XX','11.11.1977',NOW());
INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (12,1,'Eesti','Parnu','Parnu','Torukese 95','1112222');

/* SELLERS */
INSERT INTO enterprise (name,full_name, created, is_seller) VALUES ('Torupood','Torupood OY', NOW(),'Y');

INSERT INTO ent_address (subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (1,2,'Eesti','Harju','Tallinn','Akadeemia 21A','6663363');

INSERT INTO enterprise (name,full_name, created, is_seller) VALUES ('Elektripood','Elektriasjad  OY', NOW(),'Y');

INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (2,2,'Eesti','Parnu','Parnu','Torukese 95','1112222');

/* COURIRES */

INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('Kiirkuller','Kiirkuller OY', NOW(),'Y',12);

INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (3,2,'Eesti','Parnu','Parnu','Torukese 95','1112222');

INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('DPD Sweden','DPD Sweden', NOW(),'Y',20);

INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (4,2,'Rootsi','Norbotten','Pajala','Muuk 95','1112222');


INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('UPS Denmark','UPS Denmark', NOW(),'Y',15);

INSERT INTO address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (5,2,'Taani','Arhus','Pajala','Muuk 95','1112222');


INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('UPS Norway','UPS Norway', NOW(),'Y',25);

INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (6,2,'Norra','Oslo','UksNorraKoht','Viikingi 95','1112222');

INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('Kuninglikud Vedajad','Kuninglikud Vedajad', NOW(),'Y',18);

INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (7,2,'Rootsi','Stockholm','UksNorraKoht','Viikingi 95','1112222');

INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('Kiired','Kiired OY', NOW(),'Y',20);
INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (8,2,'Eesti','Harju','Tallinn','Torukese 95','1112222');

INSERT INTO enterprise (name,full_name, created, is_courier,percent_from_order) VALUES ('Tallinna Raskeveod','Tallinna Raskeveod AS', NOW(),'Y',17);
INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (9,2,'Eesti','Harju','Tallinn','Veo 67','1112222');

INSERT INTO enterprise (name,full_name, created, is_courier, percent_from_order) VALUES ('Tallinna Raskeveod','Tallinna Raskeveod AS', NOW(),'Y',16);
INSERT INTO ent_address ( subject_fk,subject_type_fk, country,county, town_village, street_address,zipcode) 
VALUES (10,2,'Eesti','Valgamaa','Suurmetas','Taseme 67','7899');

/* PRODUCTS */

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('470065-321','ProLiant ML110','HP ProLiant ML110 G6 - P G6950 2.8 GHz',400);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('S11T1102102E','Dell PowerEdge T110','Dell PowerEdge T110 - Core i3 540 3.06 GHz',450);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('AX357AW#ABB','HP Compaq 6005 Pro','HP Compaq 6005 Pro - Phenom II X2 B55 3 GHz',466);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('ML-1660/SEE','ML1660','Samsung ML 1660 printer  B/W  laser',58);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('HL2035ZW1','Brother HL 2035','Brother HL 2035 - printer - B/W - laser',97);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('7303WBU','ThinkCentre M58e','Lenovo ThinkCentre M58e 7303 ',214);

INSERT INTO product (product_code,name,description, eshop_price)
VALUES ('LE32S86BD','Samsung LE32S86BD','Samsung LE32S86BD',321);


/* ORDERS */


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (1,1,2,400,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (1,1,400,400,1,'ProLiant ML110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (1,2,2,1266,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (2,1,400,800,2,'ProLiant ML110');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (2,3,466,466,1,'HP Compaq 6005 Pro');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (1,2,2,1266,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (3,1,400,800,2,'ProLiant ML110');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (3,3,466,466,1,'HP Compaq 6005 Pro');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (2,4,2,866,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (4,1,400,400,1,'ProLiant ML110');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (4,3,466,466,1,'HP Compaq 6005 Pro');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (3,5,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (5,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (5,3,450,450,1,'Dell PowerEdge T110');


/*  */

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (5,8,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (6,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (6,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (5,8,1,900,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (6,3,214,214,1,'ThinkCentre M58e');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (10,16,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (7,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (7,3,450,450,1,'Dell PowerEdge T110');



INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (10,15,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (8,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (8,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (11,17,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (9,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (9,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (11,18,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (10,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (10,3,450,450,1,'Dell PowerEdge T110');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (4,6,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (11,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (11,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (4,7,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (12,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (12,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (5,8,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (13,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (13,3,450,450,1,'Dell PowerEdge T110');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (12,19,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (14,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (14,3,450,450,1,'Dell PowerEdge T110');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (9,14,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (15,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (15,3,450,450,1,'Dell PowerEdge T110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (8,13,2,664,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (16,3,97,97,1,'Brother HL 2035');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (16,3,450,450,1,'Dell PowerEdge T110');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (7,11,2,771,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (17,3,321,321,1,'Samsung LE32S86BD');
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (17,3,450,450,1,'Dell PowerEdge T110');


INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (6,10,2,400,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (18,1,400,400,1,'ProLiant ML110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (5,9,2,400,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (19,1,400,400,1,'ProLiant ML110');

INSERT INTO eshop_order (customer_fk, shipping_address_fk,seller_fk, price_total, confirmation_date)
VALUES (3,5,2,400,NOW());
INSERT INTO order_product (eshop_order_fk, product_fk, price, price_total, product_count,product_name)
VALUES (20,1,400,400,1,'ProLiant ML110');


