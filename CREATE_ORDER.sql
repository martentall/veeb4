CREATE SEQUENCE customer_id ;

CREATE TABLE customer 
( customer numeric(10,0) NOT NULL DEFAULT nextval('customer_id'),
  first_name varchar(100),
  last_name varchar(100),
  identity_code varchar(20),
  birth_date date,
  created_by numeric(10,0),
  updated_by numeric(10,0),
  created timestamp,
  updated timestamp,
  CONSTRAINT customer_pk PRIMARY KEY (customer)
) ;

CREATE SEQUENCE address_id ;

CREATE TABLE address 
( address numeric(10,0) NOT NULL DEFAULT nextval('address_id'),
  address_type_fk numeric(10,0),
  subject_fk numeric(10,0),
  subject_type_fk numeric(10,0),
  country varchar(50),
  county varchar(100),
  town_village varchar(100),
  street_address varchar(100),
  zipcode varchar(50),
  CONSTRAINT address_pk PRIMARY KEY (address)
) ;


CREATE SEQUENCE ent_address_id ;

CREATE TABLE ent_address 
( ent_address numeric(10,0) NOT NULL DEFAULT nextval('ent_address_id'),
  address_type_fk numeric(10,0),
  subject_fk numeric(10,0),
  subject_type_fk numeric(10,0),
  country varchar(50),
  county varchar(100),
  town_village varchar(100),
  street_address varchar(100),
  zipcode varchar(50),
  CONSTRAINT ent_address_pk PRIMARY KEY (ent_address)
) ;

CREATE SEQUENCE enterprise_id ;

CREATE TABLE enterprise
( enterprise numeric(10,0) NOT NULL DEFAULT nextval('enterprise_id'),
  name text,
  full_name text,
  created_by numeric(10,0),
  updated_by numeric(10,0),
  created timestamp,
  updated timestamp,
  is_courier varchar(1),
  is_seller varchar(1),	
  percent_from_order numeric(10,0),
  CONSTRAINT enterprise_pk PRIMARY KEY (enterprise)
) ;

CREATE SEQUENCE eshop_order_id ;

CREATE TABLE eshop_order 
( eshop_order numeric(10,0) NOT NULL DEFAULT nextval('eshop_order_id'),
  customer_fk numeric(10,0),
  confirmation_date timestamp,
  price_total numeric,
  customer_note text,
  shipping_address_fk numeric(10,0),
  seller_fk numeric(10,0),
  CONSTRAINT eshop_order_pk PRIMARY KEY (eshop_order)
) ;

CREATE SEQUENCE order_product_id ;

CREATE TABLE order_product 
( order_product numeric(10,0) NOT NULL DEFAULT nextval('order_product_id'),
  eshop_order_fk numeric(10,0),
  product_fk numeric(10,0),
  product_name text,
  product_count numeric,
  price numeric,
  price_total numeric,
  created timestamp,
  created_by numeric(10,0),
  CONSTRAINT order_product_pk PRIMARY KEY (order_product)
) ;

CREATE SEQUENCE product_id ;

CREATE TABLE product
( product numeric(10,0) NOT NULL DEFAULT nextval('product_id'),
  product_catalog_fk numeric(10,0),
  product_code text,
  name text,
  description text,
  eshop_price numeric ,
  max_customer_discount numeric(10,0),
  created timestamp,
  created_by numeric(10,0),
  updated timestamp,
  updated_by numeric(10,0),
  CONSTRAINT product_pk PRIMARY KEY (product)
) ;

CREATE SEQUENCE order_shipment_id ;

CREATE TABLE order_shipment 
( order_shipment numeric(10,0) NOT NULL DEFAULT nextval('order_shipment_id'),
  order_fk numeric(10,0),
  tracking_number text,
  courier_name text,
  shipping_price numeric,
  approx_delivery_date date,
  created timestamp DEFAULT NOW(),
  ryhm_name text,
  CONSTRAINT order_shipment_pk PRIMARY KEY (order_shipment)
) ;


/* order_shipment tabeli indeksid ja piirangud */

CREATE   INDEX order_shipment_idx1
 ON order_shipment
  (order_shipment);
  
 CREATE   INDEX order_shipment_idx2
 ON order_shipment
  (order_fk);
  