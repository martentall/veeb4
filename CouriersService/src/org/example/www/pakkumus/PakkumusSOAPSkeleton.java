/**
 * PakkumusSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

public class PakkumusSOAPSkeleton implements org.example.www.pakkumus.Pakkumus_PortType, org.apache.axis.wsdl.Skeleton {
    private org.example.www.pakkumus.Pakkumus_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "PakkumusParing"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.example.org/pakkumus/", ">PakkumusParing"), org.example.www.pakkumus.PakkumusParing.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("teePakkumus", _params, new javax.xml.namespace.QName("http://www.example.org/pakkumus/", "PakkumusVastus"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.example.org/pakkumus/", ">PakkumusVastus"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "teePakkumus"));
        _oper.setSoapAction("http://idu0080.ttu.ee/pakkumus/teePakkumus");
        _myOperationsList.add(_oper);
        if (_myOperations.get("teePakkumus") == null) {
            _myOperations.put("teePakkumus", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("teePakkumus")).add(_oper);
    }

    public PakkumusSOAPSkeleton() {
        this.impl = new org.example.www.pakkumus.PakkumusSOAPImpl();
    }

    public PakkumusSOAPSkeleton(org.example.www.pakkumus.Pakkumus_PortType impl) {
        this.impl = impl;
    }
    public org.example.www.pakkumus.PakkumusVastus teePakkumus(org.example.www.pakkumus.PakkumusParing parameters) throws java.rmi.RemoteException
    {
        org.example.www.pakkumus.PakkumusVastus ret = impl.teePakkumus(parameters);
        return ret;
    }

}
