/**
 * PakkumusSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.pakkumus;

import program.PakkumusVastusCreator;
import ttu.idu0080.order.server.Courier;
import ttu.idu0080.order.server.CourierServiceProxy;

public class PakkumusSOAPImpl implements org.example.www.pakkumus.Pakkumus_PortType{
    public org.example.www.pakkumus.PakkumusVastus teePakkumus(org.example.www.pakkumus.PakkumusParing parameters) throws java.rmi.RemoteException {
    	CourierServiceProxy courierProxy = new CourierServiceProxy();
    	Courier courier = courierProxy.getCourierById(parameters.getKuller_id());
        return new PakkumusVastusCreator().create(parameters, courier);
    }
}
