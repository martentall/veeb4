/**
 * CourierServiceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class CourierServiceServiceLocator extends org.apache.axis.client.Service implements ttu.idu0080.order.server.CourierServiceService {

    public CourierServiceServiceLocator() {
    }


    public CourierServiceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CourierServiceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CourierServicePort
    private java.lang.String CourierServicePort_address = "http://localhost:8080/TtuServices/services/CourierServicePort";

    public java.lang.String getCourierServicePortAddress() {
        return CourierServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CourierServicePortWSDDServiceName = "CourierServicePort";

    public java.lang.String getCourierServicePortWSDDServiceName() {
        return CourierServicePortWSDDServiceName;
    }

    public void setCourierServicePortWSDDServiceName(java.lang.String name) {
        CourierServicePortWSDDServiceName = name;
    }

    public ttu.idu0080.order.server.CourierService getCourierServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CourierServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCourierServicePort(endpoint);
    }

    public ttu.idu0080.order.server.CourierService getCourierServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ttu.idu0080.order.server.CourierServicePortBindingStub _stub = new ttu.idu0080.order.server.CourierServicePortBindingStub(portAddress, this);
            _stub.setPortName(getCourierServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCourierServicePortEndpointAddress(java.lang.String address) {
        CourierServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ttu.idu0080.order.server.CourierService.class.isAssignableFrom(serviceEndpointInterface)) {
                ttu.idu0080.order.server.CourierServicePortBindingStub _stub = new ttu.idu0080.order.server.CourierServicePortBindingStub(new java.net.URL(CourierServicePort_address), this);
                _stub.setPortName(getCourierServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CourierServicePort".equals(inputPortName)) {
            return getCourierServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "CourierServiceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "CourierServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CourierServicePort".equals(portName)) {
            setCourierServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
