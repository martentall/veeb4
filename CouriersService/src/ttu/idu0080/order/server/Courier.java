/**
 * Courier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ttu.idu0080.order.server;

public class Courier  implements java.io.Serializable {
    private ttu.idu0080.order.server.EntAddress[] addresses;

    private int enterprise;

    private java.lang.String name;

    private int percent_from_order;

    public Courier() {
    }

    public Courier(
           ttu.idu0080.order.server.EntAddress[] addresses,
           int enterprise,
           java.lang.String name,
           int percent_from_order) {
           this.addresses = addresses;
           this.enterprise = enterprise;
           this.name = name;
           this.percent_from_order = percent_from_order;
    }


    /**
     * Gets the addresses value for this Courier.
     * 
     * @return addresses
     */
    public ttu.idu0080.order.server.EntAddress[] getAddresses() {
        return addresses;
    }


    /**
     * Sets the addresses value for this Courier.
     * 
     * @param addresses
     */
    public void setAddresses(ttu.idu0080.order.server.EntAddress[] addresses) {
        this.addresses = addresses;
    }

    public ttu.idu0080.order.server.EntAddress getAddresses(int i) {
        return this.addresses[i];
    }

    public void setAddresses(int i, ttu.idu0080.order.server.EntAddress _value) {
        this.addresses[i] = _value;
    }


    /**
     * Gets the enterprise value for this Courier.
     * 
     * @return enterprise
     */
    public int getEnterprise() {
        return enterprise;
    }


    /**
     * Sets the enterprise value for this Courier.
     * 
     * @param enterprise
     */
    public void setEnterprise(int enterprise) {
        this.enterprise = enterprise;
    }


    /**
     * Gets the name value for this Courier.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Courier.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the percent_from_order value for this Courier.
     * 
     * @return percent_from_order
     */
    public int getPercent_from_order() {
        return percent_from_order;
    }


    /**
     * Sets the percent_from_order value for this Courier.
     * 
     * @param percent_from_order
     */
    public void setPercent_from_order(int percent_from_order) {
        this.percent_from_order = percent_from_order;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Courier)) return false;
        Courier other = (Courier) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addresses==null && other.getAddresses()==null) || 
             (this.addresses!=null &&
              java.util.Arrays.equals(this.addresses, other.getAddresses()))) &&
            this.enterprise == other.getEnterprise() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            this.percent_from_order == other.getPercent_from_order();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddresses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAddresses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAddresses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getEnterprise();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        _hashCode += getPercent_from_order();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Courier.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "courier"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addresses");
        elemField.setXmlName(new javax.xml.namespace.QName("", "addresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://server.order.idu0080.ttu/", "entAddress"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enterprise");
        elemField.setXmlName(new javax.xml.namespace.QName("", "enterprise"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("percent_from_order");
        elemField.setXmlName(new javax.xml.namespace.QName("", "percent_from_order"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
