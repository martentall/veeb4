package ttu.idu0080.order.server;

public class CourierServiceProxy implements ttu.idu0080.order.server.CourierService {
  private String _endpoint = null;
  private ttu.idu0080.order.server.CourierService courierService = null;
  
  public CourierServiceProxy() {
    _initCourierServiceProxy();
  }
  
  public CourierServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initCourierServiceProxy();
  }
  
  private void _initCourierServiceProxy() {
    try {
      courierService = (new ttu.idu0080.order.server.CourierServiceServiceLocator()).getCourierServicePort();
      if (courierService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)courierService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)courierService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (courierService != null)
      ((javax.xml.rpc.Stub)courierService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ttu.idu0080.order.server.CourierService getCourierService() {
    if (courierService == null)
      _initCourierServiceProxy();
    return courierService;
  }
  
  public ttu.idu0080.order.server.Courier[] getCouriersByAddress(java.lang.String country, java.lang.String county) throws java.rmi.RemoteException{
    if (courierService == null)
      _initCourierServiceProxy();
    return courierService.getCouriersByAddress(country, county);
  }
  
  public ttu.idu0080.order.server.Courier[] getAllCouriers() throws java.rmi.RemoteException{
    if (courierService == null)
      _initCourierServiceProxy();
    return courierService.getAllCouriers();
  }
  
  public ttu.idu0080.order.server.Courier getCourierById(int courier_id) throws java.rmi.RemoteException{
    if (courierService == null)
      _initCourierServiceProxy();
    return courierService.getCourierById(courier_id);
  }
  
  
}