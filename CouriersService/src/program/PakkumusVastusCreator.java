package program;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import org.example.www.pakkumus.Address;
import org.example.www.pakkumus.PakkumusParing;
import org.example.www.pakkumus.PakkumusVastus;

import ttu.idu0080.order.server.Courier;

public class PakkumusVastusCreator {

	public PakkumusVastus create(PakkumusParing parameters, Courier courier) {
		PakkumusVastus vastus = new PakkumusVastus();
		vastus.setHind(calculatePrice(parameters, courier));
		vastus.setPaevade_arv(getRandomPaevaArv());
		vastus.setOffer_id(getRandomOfferId());
		return vastus;
	}
	
	private String getRandomOfferId() {
		return new BigInteger(130, new SecureRandom()).toString(32).substring(0, 10).toUpperCase();
	}

	private Integer getRandomPaevaArv() {
		return new Random().nextInt(8-1) + 1;
	}
	
	private double calculatePrice(PakkumusParing parameters, Courier courier) {
		double carrierFullPrice = calculateCourierPrice(parameters, courier);
		
		String orderCounty = getOrderCounty(parameters);
		Address[] sellerAdresses = parameters.getTellimus().getSeller().getAddresses();
		double discount = getDiscount(courier, orderCounty, sellerAdresses);
		
		return calculatePriceWithDiscount(carrierFullPrice, discount);
	}

	private String getOrderCounty(PakkumusParing parameters) {
		if (parameters.getTellimus().getShipping_address() == null) return null;
		return parameters.getTellimus().getShipping_address().getCounty();
	}

	private double getDiscount(Courier courier, String orderCounty,
			Address[] sellerAdresses) {
		return getAddressMatchesOrderAddressDiscount(courier, orderCounty) +
				getSellerAddressMatchesOrderAddressDiscount(sellerAdresses, orderCounty);
	}

	private double getAddressMatchesOrderAddressDiscount(Courier courier, String orderCounty) {
		if (courier.getAddresses() == null) return 0.0;
		for (int i = 0; i < courier.getAddresses().length; i++) {
			if (addressesMatch(courier.getAddresses()[i].getCounty(), orderCounty)) {
				return 0.3;
			}
		}
		return 0.0;
	}
	
	private double getSellerAddressMatchesOrderAddressDiscount(Address[] sellerAdresses, String orderCounty) {
		if (sellerAdresses == null) return 0.0;
		for (int j = 0; j < sellerAdresses.length; j++) {
			if (addressesMatch(sellerAdresses[j].getCounty(), orderCounty)) {
				return 0.3;
			}
		}
		return 0.0;
	}

	private double calculatePriceWithDiscount(double carrierFullPrice, double discount) {
		return (1.0 - discount) * carrierFullPrice;
	}

	private boolean addressesMatch(String county, String orderCounty) {
		if (county == null) return false;
		return county.equalsIgnoreCase(orderCounty);
	}

	private double calculateCourierPrice(
			org.example.www.pakkumus.PakkumusParing parameters, Courier courier) {
		return parameters.getTellimus().getPrice_total() * courier.getPercent_from_order() / 100;
	}

}
