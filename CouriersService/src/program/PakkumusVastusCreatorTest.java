package program;

import static org.junit.Assert.*;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.example.www.pakkumus.Address;
import org.example.www.pakkumus.PakkumusParing;
import org.example.www.pakkumus.PakkumusVastus;
import org.example.www.pakkumus.Seller;
import org.example.www.pakkumus.TellimusType;
import org.junit.Test;

import ttu.idu0080.order.server.Courier;
import ttu.idu0080.order.server.EntAddress;

public class PakkumusVastusCreatorTest {

	@Test
	public void testHasDiscountCourierAddress() {
		
		PakkumusParing parameters = new PakkumusParing();
		parameters.setKuller_id(1);
		
		TellimusType tellimus = createTellimus("testCounty");
		tellimus.setSeller(createSellerWithCounty("asdasd"));
		parameters.setTellimus(tellimus);
		
		Courier courier = createCourier();
		
		PakkumusVastus vastus = new PakkumusVastusCreator().create(parameters, courier);
		assertEquals(parameters.getTellimus().getPrice_total()*0.7, vastus.getHind(), 0.01);
	}

	private Courier createCourier() {
		Courier courier = new Courier();
		courier.setAddresses(createAddresses());
		courier.setPercent_from_order(100);
		return courier;
	}
	
	@Test
	public void testHasDiscountSellerAddress() {
		
		PakkumusParing parameters = new PakkumusParing();
		parameters.setKuller_id(1);
		
		TellimusType tellimus = createTellimus("asdasd");
		tellimus.setSeller(createSellerWithCounty("asdasd"));
		parameters.setTellimus(tellimus);
		
		Courier courier = createCourier();
		
		PakkumusVastus vastus = new PakkumusVastusCreator().create(parameters, courier);
		assertEquals(parameters.getTellimus().getPrice_total()*0.7, vastus.getHind(), 0.01);
	}

	@Test
	public void testNoDiscount() {
		
		PakkumusParing parameters = new PakkumusParing();
		parameters.setKuller_id(1);
		
		TellimusType tellimus = createTellimus("asdasd");
		tellimus.setSeller(createSellerWithCounty("not_same"));
		parameters.setTellimus(tellimus);
		
		Courier courier = createCourier();
		
		PakkumusVastus vastus = new PakkumusVastusCreator().create(parameters, courier);
		assertEquals(parameters.getTellimus().getPrice_total(), vastus.getHind(), 0.01);
	}
	
	@Test
	public void testBothDiscounts() {
		
		PakkumusParing parameters = new PakkumusParing();
		parameters.setKuller_id(1);
		
		TellimusType tellimus = createTellimus("testCounty");
		tellimus.setSeller(createSellerWithCounty("testCounty"));
		parameters.setTellimus(tellimus);
		
		Courier courier = createCourier();
		
		PakkumusVastus vastus = new PakkumusVastusCreator().create(parameters, courier);
		assertEquals(parameters.getTellimus().getPrice_total()*0.4, vastus.getHind(), 0.01);
	}
	
	private EntAddress[] createAddresses() {
		EntAddress[] addresses = new EntAddress[2];
		
		EntAddress addr1 = new EntAddress();
		addr1.setCounty("testCounty");
		
		EntAddress addr2 = new EntAddress();
		addr2.setCounty("randomCounty");
		
		addresses[0] = addr1;
		addresses[1] = addr2;
		return addresses;
	}

	private TellimusType createTellimus(String county) {
		TellimusType tellimus = new TellimusType();
		Address shippingAddress = new Address();
		shippingAddress.setCounty(county);
		tellimus.setShipping_address(shippingAddress);
		tellimus.setPrice_total(100);
		return tellimus;
	}

	private Seller createSellerWithCounty(String county) {
		Seller seller = new Seller();
		seller.setAddresses(createSellerAddress(county));
		return seller;
	}

	private Address[] createSellerAddress(String county) {
		Address[] addresses = new Address[1];
		
		Address addr1 = new Address();
		addr1.setCounty(county);
		
		addresses[0] = addr1;
		return addresses;
	}

}
