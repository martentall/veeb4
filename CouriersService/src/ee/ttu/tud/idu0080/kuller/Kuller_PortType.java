/**
 * Kuller_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.kuller;

public interface Kuller_PortType extends java.rmi.Remote {
    public ee.ttu.tud.idu0080.kuller.Kuller_Type[] getKullerid() throws java.rmi.RemoteException;
}
