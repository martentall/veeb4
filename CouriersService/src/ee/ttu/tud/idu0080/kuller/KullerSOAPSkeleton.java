/**
 * KullerSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.kuller;

public class KullerSOAPSkeleton implements ee.ttu.tud.idu0080.kuller.Kuller_PortType, org.apache.axis.wsdl.Skeleton {
    private ee.ttu.tud.idu0080.kuller.Kuller_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getKullerid", _params, new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/kuller/", "KulleridVastus"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/kuller/", ">KulleridVastus"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "getKullerid"));
        _oper.setSoapAction("http://idu0080.tud.ttu.ee/kuller/getKullerid");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getKullerid") == null) {
            _myOperations.put("getKullerid", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getKullerid")).add(_oper);
    }

    public KullerSOAPSkeleton() {
        this.impl = new ee.ttu.tud.idu0080.kuller.KullerSOAPImpl();
    }

    public KullerSOAPSkeleton(ee.ttu.tud.idu0080.kuller.Kuller_PortType impl) {
        this.impl = impl;
    }
    public ee.ttu.tud.idu0080.kuller.Kuller_Type[] getKullerid() throws java.rmi.RemoteException
    {
        ee.ttu.tud.idu0080.kuller.Kuller_Type[] ret = impl.getKullerid();
        return ret;
    }

}
