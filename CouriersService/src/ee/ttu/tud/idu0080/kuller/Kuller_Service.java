/**
 * Kuller_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.kuller;

public interface Kuller_Service extends javax.xml.rpc.Service {
    public java.lang.String getkullerSOAPAddress();

    public ee.ttu.tud.idu0080.kuller.Kuller_PortType getkullerSOAP() throws javax.xml.rpc.ServiceException;

    public ee.ttu.tud.idu0080.kuller.Kuller_PortType getkullerSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
