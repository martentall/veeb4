/**
 * KullerSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.kuller;

import ttu.idu0080.order.server.Courier;
import ttu.idu0080.order.server.CourierServiceProxy;

public class KullerSOAPImpl implements ee.ttu.tud.idu0080.kuller.Kuller_PortType{
    public ee.ttu.tud.idu0080.kuller.Kuller_Type[] getKullerid() throws java.rmi.RemoteException {
    	Kuller_Type[] kullers = null;
    	
    	Courier[] couriers = new CourierServiceProxy().getAllCouriers();
    	if (couriers != null) {
        	kullers = new Kuller_Type[couriers.length];
        	for (int i = 0; i < couriers.length; i++) {
        		Kuller_Type kuller = new Kuller_Type();
        		kuller.setKuller_id(couriers[i].getEnterprise());
        		kuller.setNimi(couriers[i].getName());
    			kullers[i] = kuller;
    		}    	
        	return kullers;
    	}
        return new Kuller_Type[0];
    }

}
