/**
 * Transport_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.transport;

public interface Transport_Service extends javax.xml.rpc.Service {
    public java.lang.String gettransportSOAPAddress();

    public ee.ttu.tud.idu0080.transport.Transport_PortType gettransportSOAP() throws javax.xml.rpc.ServiceException;

    public ee.ttu.tud.idu0080.transport.Transport_PortType gettransportSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
