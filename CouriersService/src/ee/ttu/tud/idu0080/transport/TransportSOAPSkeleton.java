/**
 * TransportSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.transport;

public class TransportSOAPSkeleton implements ee.ttu.tud.idu0080.transport.Transport_PortType, org.apache.axis.wsdl.Skeleton {
    private ee.ttu.tud.idu0080.transport.Transport_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "pakkumus_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("transport", _params, new javax.xml.namespace.QName("", "track_id"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://idu0080.tud.ttu.ee/transport/", "Transport"));
        _oper.setSoapAction("http://idu0080.ttu.ee/transport/Transport");
        _myOperationsList.add(_oper);
        if (_myOperations.get("transport") == null) {
            _myOperations.put("transport", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("transport")).add(_oper);
    }

    public TransportSOAPSkeleton() {
        this.impl = new ee.ttu.tud.idu0080.transport.TransportSOAPImpl();
    }

    public TransportSOAPSkeleton(ee.ttu.tud.idu0080.transport.Transport_PortType impl) {
        this.impl = impl;
    }
    public java.lang.String transport(java.lang.String pakkumus_id) throws java.rmi.RemoteException
    {
        java.lang.String ret = impl.transport(pakkumus_id);
        return ret;
    }

}
