/**
 * TransportSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ee.ttu.tud.idu0080.transport;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TransportSOAPImpl implements ee.ttu.tud.idu0080.transport.Transport_PortType{
    private static final String SEPARATOR = "#";

	public java.lang.String transport(java.lang.String pakkumus_id) throws java.rmi.RemoteException {
        return pakkumus_id.toLowerCase() + SEPARATOR  + getFormattedDate();
    }

	private String getFormattedDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		return formatter.format(LocalDate.now());
	}

}
